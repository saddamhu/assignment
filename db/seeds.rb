
Product.create(name: 'iphone 8')
Product.create(name: 'iphone 7')
Color.create(name: 'Gold', color_code: '#ffd700')
Color.create(name: 'Grey', color_code: '#d3d3d3')
Storage.create(name: '64GB')
Storage.create(name: '256GB')
Variant.create!(color_id: 1,
	              storage_id: 1, 
	              product_id: 1, 
	              price: 64000,
	              description: 'iOS v11 operating system with 1.2GHz Apple A11 Bionic hexa core processor, 2GB RAM, 64GB internal memory and single SIM')

Variant.create!(color_id: 2,
	              storage_id: 2, 
	              product_id: 1, 
	              price: 70000,
	              description: 'iOS v11 operating system with 1.2GHz Apple A11 Bionic hexa core processor, 2GB RAM, 256GB internal memory and single SIM')

Variant.create!(color_id: 1,
	              storage_id: 1, 
	              product_id: 2, 
	              price: 46000,
	              description: '12MP primary camera with optical image stabilisation, quad-LED true tone flash and live photos, 4K video recording at 30 fps and slow-motion video recording in 1080p at 120 fps and 7MP front facing camera')