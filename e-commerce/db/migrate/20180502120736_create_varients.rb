class CreateVarients < ActiveRecord::Migration[5.1]
  def change
    create_table :varients do |t|
    	t.integer :product_id
    	t.integer :color_id
    	t.integer :storage_id
    	t.integer :price

      t.timestamps
    end
  end
end
