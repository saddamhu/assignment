# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Storage.create(capacity: '64GB')
Storage.create(capacity: '256GB')

Color.create(name: 'white', color_code: '#ffffff')
Color.create(name: 'black', color_code: '#000000')

Varient.create(storage_id: 1, color_id: 1, price: 58468)
Varient.create(storage_id: 1, color_id: 2, price: 59990)
Varient.create(storage_id: 2, color_id: 1, price: 69999)
Varient.create(storage_id: 2, color_id: 2, price: 69999)


