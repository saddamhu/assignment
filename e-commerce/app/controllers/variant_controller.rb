class VariantController < ApplicationController
	before_action :set_variant, only: [:show, :update, :destroy]

  # GET /variants
  def index
    @variants = Variant.all
    json_response(@variants)
  end

  # POST /variants
  def create
    @variant = Variant.create!(variant_params)
    json_response(@variant, :created)
  end

  # GET /variants/:id
  def show
    json_response(@variant)
  end

  # PUT /variants/:id
  def update
    @variant.update(variant_params)
    head :no_content
  end

  # DELETE /variants/:id
  def destroy
    @variant.destroy
    head :no_content
  end

  private

  def variant_params
    # whitelist params
    params.permit(:title, :created_by)
  end

  def set_variant
    @variant = Variant.find(params[:id])
  end
end
