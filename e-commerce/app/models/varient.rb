class Varient < ApplicationRecord
	has_one :color
	has_one :storage
	belongs_to :product
end
