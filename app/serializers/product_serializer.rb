class ProductSerializer < ActiveModel::Serializer
  attributes :id, 
             :name,
             :variants

  def variants
  	variant_list = []
  	object.variants.each do |variant|
  		product_variants = {}
      product_variants['id'] = variant.id
      product_variants['color'] = variant.color.name
      product_variants['color_code'] = variant.color.color_code
      product_variants['storage'] = variant.storage.name
      product_variants['price'] = variant.price
      product_variants['description'] = variant.description
      variant_list << product_variants
  	end
  	variant_list
  end
end
