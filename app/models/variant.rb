class Variant < ApplicationRecord
  belongs_to :product
  belongs_to :color
  belongs_to :storage
end
