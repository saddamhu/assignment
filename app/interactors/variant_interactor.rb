class VariantInteractor
  
  def self.create_product(params)
  	product = Product.create!(name: params[:name])
  	params[:variants].each do |variant|
  		color = Color.find_or_create_by(variant[:color])
  		storage = Storage.find_or_create_by(variant[:storage])
  		Variant.create!(color_id: color.id,
  			              storage_id: storage.id, 
  			              product_id: product.id, 
  			              price: variant[:price],
  			              description: variant[:description])
  	end
    product
  end

  def self.update_product(params,product)
  	params[:variants].each do |variant|
  		color = Color.find_or_create_by(name: variant[:color], color_code: variant[:color_code])
  		storage = Storage.find_or_create_by(name: variant[:storage])
  		variant = Variant.find(variant[:id])
  		variant.update!(color_id: color.id,
  			              storage_id: storage.id, 
  			              product_id: product.id, 
  			              price: variant[:price],
  			              description: variant[:description])
  	end
    product
  end
end