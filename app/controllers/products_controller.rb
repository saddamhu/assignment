class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy]

  # GET /products
  def index
    @products = Product.all

    render json: @products, each_serializer: ProductSerializer
  end

  # GET /products/1
  def show
    render json: @product
  end

  # POST /products
  def create
    @product = VariantInteractor.create_product(product_params)

    if @product
      render json: @product, status: :created, location: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1
  def update
    @product = VariantInteractor.update_product(product_params,@product)
    if @product
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit!.except(:id)
    end
end
